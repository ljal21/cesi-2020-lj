# TP1 (Avtan;Theo;Leo;Alex)

## I. Utilisateurs

### 1. Création et configuration

* Création de l'user
``` bash

$ sudo useradd toto -m -s /bin/bash -u 3000
$ sudo passwd toto
[usr@localhost home]$ ls
toto  theo
```
* Création du groupe
``` bash
$ sudo groupadd admins
```

* Pour permettre à ce groupe d'accéder aux droits root
``` bash
$ sudo visudo 
"%admins ALL=(ALL)       ALL"
$ usermod -aG admins toto
$ groups toto
[toto@localhost home] toto : toto admins
```
### 2.SSH

``` bash

$ cd /home/<USER>
$ mkdir .ssh
$ chown -R toto:toto .ssh/
$ touch .ssh/authorized_keys

$ echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCUo7aceNDHy6Bb0fzD0Izyi7IhPGWp8LiAIw+zcFHDF6hBxijzl+3kESLrGWPaCAlXOkB/TVwqWSsjqsY6i94U0p5gtUuKZNNAlPGnDL9Fc4kVqyLqYpHt3N70NgbkLAGIcfPDUjP5qnvNxAMNpzA/B/eP1I7n6+s5CAKOnjSGTBjije+hZe5J+4Ad0KMshJ//S35LgBgrrXn4IH1fJsQvO85bRorN2N+it4GUmub6PLHLU7FauZVaPxDpcexwya64TcxurX2OmJZAPdggHvMo5Lhz8FzGuUMcUzecTPJ8Dt3rFyL5R9oEfTDqJ28VQafA0dX12H3anCg+BpsmoS3fwlEqOYqFuhfrZaysmM/EJ0eroHs25DZC90o+Lvv7fb29kT5Kj+TOaJblp3nvGgRs5nKatJgRD89+a5PEoMSUt1NR6EWHqBp+9IkXPk0M+2Iex3QYYzFXY5CsVouUF6Osz/i5s6B2XAIZkP14x7fUv81Q3W4LIfSeAmpJBMmHGlWedunjfT4rT/LAh6l7DnuSqhts88cfrB0AJiheieedQkNsHPbDJ8iZ/hPeg+1aURPSIgA/6np8t6V/oJ/B9kCvq4brkOLF0dO/CVvGTG0vU7TI+C1jO6CCqS1n6XfoccnnddC8/OPyEESQRAjln89Kj/yHSVxnt3UO2Ej4HZuwCw== leoje@DESKTOP-B40US5C' > .ssh/authorized_keys

$ chmod 700 .ssh -R
$ chmod 600 .ssh/authorized_keys
```

## II. Configuration réseau

### 1. Définir un nom de domaine à la machine

``` bash

$ sudo hostname Igorette
$ hostname
Igorette
```

### 2. Serveur DNS
``` bash
$ sudo vim /etc/sysconfig/network-scripts/*carteréseau*
"DNS=1.1.1.1"
```
## II. Partitionnement

### 1. Partitionnement
* Nos 2 disques de 3G ont étés rajoutés
``` bash
$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part
  ├─centos-root 253:0    0  6.2G  0 lvm  /
  └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0    3G  0 disk
sdc               8:32   0    3G  0 disk
sr0              11:0    1  973M  0 rom
```

``` bash
$ sudo pvcreate /dev/sdb
$ sudo pvcreate /dev/sdc
$ sudo pvs
PV         VG     Fmt  Attr PSize   PFree
  /dev/sda2  centos lvm2 a--  <19.00g    0
  /dev/sdb          lvm2 ---    3.00g 3.00g
  /dev/sdc          lvm2 ---    3.00g 3.00g
```
* Agregation des deux disques crées en un seul volume group
``` bash
$ sudo vgcreate cesi-data1 /dev/sdb
$ sudo vgs 
 VG         #PV #LV #SN Attr   VSize   VFree
  centos       1   2   0 wz--n- <19.00g     0
  cesi-data1   1   0   0 wz--n-  <3.00g <3.00g
```
``` bash
$ sudo vgextend cesi-data1 /dev/sdc
$ sudo vgs
  VG         #PV #LV #SN Attr   VSize   VFree
  centos       1   2   0 wz--n- <19.00g    0
  cesi-data1   2   0   0 wz--n-   5.99g 5.99g
```
* Création des 3 logical volumes de 1Go
``` bash
$ sudo lvcreate -L 1G cesi-data1 -n lv1
$ sudo lvcreate -L 1G cesi-data1 -n lv2
$ sudo lvcreate -L 1G cesi-data1 -n lv3
```
* Formatage en ext4
``` bash
$ sudo mkfs -t ext4 /dev/cesi-data1/lv1
$ sudo mkfs -t ext4 /dev/cesi-data1/lv2
$ sudo mkfs -t ext4 /dev/cesi-data1/lv3

```
* Montage des partitions
``` bash
$ sudo mkdir /mnt/part1
$ sudo mkdir /mnt/part2
$ sudo mkdir /mnt/part3
``` 

``` bash
$ sudo mount /dev/cesi-data1/lv1 /mnt/part1
$ sudo mount /dev/cesi-data1/lv2 /mnt/part2
$ sudo mount /dev/cesi-data1/lv3 /mnt/part3
```
* Le fichier /etc/fstab va nous permettre de monter automatiquement les partitions au démarrage du système
``` bash
$ sudo vim /etc/fstab
$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Tue Dec 15 09:09:48 2020
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=f928f163-3059-44ce-8076-c414fa3d040c /boot                   xfs     defaults        0 0
/dev/mapper/centos-swap swap                    swap    defaults        0 0
/dev/cesi-data1/lv1 /mnt/part1 ext4 defaults 0 0
/dev/cesi-data1/lv2 /mnt/part2 ext4 defaults 0 0
/dev/cesi-data1/lv3 /mnt/part3 ext4 defaults 0 0
```


## IV. Gestion de services

### 1. Interaction avec un service existant

``` bash
$ systemctl status firewalld

● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2020-12-15 14:21:58 CET; 34min ago
     Docs: man:firewalld(1)
 Main PID: 710 (firewalld)
   CGroup: /system.slice/firewalld.service
           └─710 /usr/bin/python2 -Es /usr/sbin/firewalld --nofork --nopid

Dec 15 14:21:57 localhost.localdomain systemd[1]: Starting firewalld - dynamic firewall daemon...
Dec 15 14:21:58 localhost.localdomain systemd[1]: Started firewalld - dynamic firewall daemon.
Dec 15 14:21:58 localhost.localdomain firewalld[710]: WARNING: AllowZoneDrifting is enabled. This is considered an inse... now.
Hint: Some lines were ellipsized, use -l to show in full.
``` 
``` bash
$ sudo vi /etc/systemd/system/web.service
$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888

[Install]
WantedBy=multi-user.target

```

 * Ouverture port 8080
``` bash
$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
$ sudo firewall-cmd --reload
success
```
### 2. Création de service

* Unité simpliste

``` bash
$ sudo systemctl daemon-reload
$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
$ sudo systemctl start web
$ sudo systemctl enable web
● Created symlink from /etc/systemd/system/multi-user.target.wants/web.service to /etc/systemd/system/web.service.
$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2020-12-15 15:04:39 CET; 8s ago
 Main PID: 2152 (python2)
   CGroup: /system.slice/web.service
           └─2152 /bin/python2 -m SimpleHTTPServer 8888
```

```bash
$ curl 10.55.55.10:8888

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="bin/">bin@</a>
<li><a href="boot/">boot/</a>
<li><a href="dev/">dev/</a>
<li><a href="etc/">etc/</a>
<li><a href="home/">home/</a>
<li><a href="lib/">lib@</a>
<li><a href="lib64/">lib64@</a>
<li><a href="media/">media/</a>
<li><a href="mnt/">mnt/</a>
<li><a href="opt/">opt/</a>
<li><a href="proc/">proc/</a>
<li><a href="root/">root/</a>
<li><a href="run/">run/</a>
<li><a href="sbin/">sbin@</a>
<li><a href="srv/">srv/</a>
<li><a href="sys/">sys/</a>
<li><a href="tmp/">tmp/</a>
<li><a href="usr/">usr/</a>
<li><a href="var/">var/</a>
</ul>
<hr>
</body>
</html>
```

* Modification de l'unité

```bash
$ sudo useradd web
$ sudo vi /etc/systemd/system/web.service
User=web
WorkingDirectory=/srv
$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888
User=web
WorkingDirectory=/srv

[Install]
WantedBy=multi-user.target

$ sudo vim /srv/test
$ sudo chown web -R /srv
$ sudo systemctl daemon-reload
$ sudo systemctl restart web
$ curl 10.55.55.10:8888
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="test">test</a>
</ul>
<hr>
</body>
</html>


```

           
           
           











