# TP2 : Serveur Web (KETSBAIA Avtandil, Schreck Alexandre,JEHANNIN Leo,MALEVRE Theo)

## I. Base De Données

* Installation et lancement Mariadb

``` bash
$ sudo setenforce 0
$ sudo yum install mariadb-server
$ sudo systemctl start mariadb
$ sudo systemctl enable mariadb
$ sudo systemctl status mariadb
"Active: active (running) since Wed 2020-12-16 10:15:49 CET; 49s ago"
```
* Mise en place d'un mot de passe pour Mariadb et autres options de sécurité via un script
``` bash
$ sudo mysql_secure_installation
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


Remove anonymous users? [Y/n] y
 ... Success!


Disallow root login remotely? [Y/n] n
 ... skipping.


Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!


Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.
```

``` bash
$ mysql -u root -p
Enter password:
```
* Création BDD et utilisateur avec tous les privilèges
```sql=
> CREATE DATABASE wordpress;
> USE wordpress;
> CREATE USER bdd@10.55.55.12 IDENTIFIED BY 'bdd';
Query OK, 0 rows affected (0.00 sec)

> MariaDB [wordpress]> GRANT ALL PRIVILEGES ON wordpress.* TO bdd@10.55.55.12 IDENTIFIED BY 'bdd';
Query OK, 0 rows affected (0.00 sec)

> FLUSH PRIVILEGES;
```
* Ouverture du port sur le firewall
```bash=
$ sudo firewall-cmd --permanent --zone=public --add-service=http 
​
$ sudo firewall-cmd --permanent --zone=public --add-service=https
​
$ sudo firewall-cmd --reload

$ sudo ss -alnpt
"LISTEN     0      50           *:3306"

$ sudo firewall-cmd --add-port=3306/tcp --permanent
success

$systemctl restart firewalld
```
## II. Serveur Web

* Installation des pré-requis et des outils pour télécharger/installer wordpress et les services web

```bash=
$ sudo yum install httpd
"Complete!"

# Installation de dépôt additionels
$ sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
$ sudo yum install -y yum-utils

# On supprime d'éventuelles vieilles versions de PHP précédemment installées
$ sudo yum remove -y php

# Activation du nouveau dépôt
$ sudo yum-config-manager --enable remi-php56  

# Installation de PHP 5.6.40 et de librairies récurrentes
$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y

$ sudo yum install wget
$ wget http://wordpress.org/latest.tar.gz

$ tar xzvf /home/user/latest.tar.gz
$ sudo mv /home/user/wordpress/* /var/www/html/

$ sudo systemctl start httpd
$ sudo systemctl enable httpd
$ sudo systemctl status httpd
 httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-12-16 10:48:12 CET; 25s ago
     Docs: man:httpd(8)
           man:apachectl(8)
 Main PID: 6106 (httpd)
   Status: "Total requests: 0; Current requests/sec: 0; Current traffic:   0 B/sec"
   CGroup: /system.slice/httpd.service
           ├─6106 /usr/sbin/httpd -DFOREGROUND
           ├─6107 /usr/sbin/httpd -DFOREGROUND
           ├─6108 /usr/sbin/httpd -DFOREGROUND
           ├─6109 /usr/sbin/httpd -DFOREGROUND
           ├─6110 /usr/sbin/httpd -DFOREGROUND
           └─6111 /usr/sbin/httpd -DFOREGROUND

Dec 16 10:48:12 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 16 10:48:12 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.

```

``` bash


```
## III. Reverse Proxy

- Installation et mise en marche de nginx et des dépôts epel necessaire à la mise en fonction du proxy

``` bash 
$ sudo yum install epel-release
$ sudo yum update 

$ sudo yum install nginx 
$ sudo systemctl start nginx && systemctl enable nginx
```
- Configuration du Firewall
```bash=

$ sudo firewall-cmd --permanent --zone=public --add-service=http
$ sudo firewall-cmd --permanent --zone=public --add-service=https
$ sudo firewall-cmd --reload
```

- Configuration

La configuration du fichier nginx se trouve dans /etc/nginx/nginx.conf
```bash=
$ sudo vim /etc/nginx/nginx.conf
events {}

http {
   server {
        listen       80;

        server_name web.cesi;

        location / {
            proxy_pass   http://10.55.55.12;
        }
    }
}
```
- Vérifier la cohérence du fichier
```bash=
$ sudo nginx -t -c /etc/nginx/nginx.conf
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
````



## IV. Un peu de sécu

- Fail2ban

```bash    

#Installation fail2ban
$ sudo yum install epel-release
$ sudo yum install fail2ban
"Complete!"

$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.d/customisation.local

#Paramétrage du fail2ban 

$ sudo vim /etc/fail2ban/jail.d/customisation.local

#Fichier de configuration pour sshd

$ sudo vim /etc/fail2ban/jail.d/sshd.conf
$ sudo cat /etc/fail2ban/jail.d/sshd.conf
# Activation de la règle
enabled = true
# nom ou numéro du port
port = ssh
# Nom du filtre à utiliser (règles de détection)
filter = sshd
# Chemin du fichier de logs
logpath = /var/log/secure
# Actions à effectuer (Voir fichier /etc/fail2ban/jail.d/customisation.local)
action = %(action_mw)s
# Durée du blocage en seconde
bantime = 3600
#Durée de surveillance en seconde
findtime = 3600
# Nombre de tentative avant blocage
maxretry = 3

# Nom de la jail
[sshd-ddos]
# Activation de la règle
enabled = true
# nom ou numéro du port
port = ssh
# nom du filtre à utiliser (regles de detection)
filter = sshd-ddos
# Chemin du fichier de logs
logpath = /var/log/secure
# Actions à effectuer (Voir fichier /etc/fail2ban/jail.d/customisation.local)
action = %(action_mw)s
# Durée du blocage en seconde
bantime = 3600
# Durée de surveillance en seconde
findtime = 3600
# Nombre de tentative avant blocage
maxretry = 3

$ sudo systemctl start fail2ban.service
$ sudo systemctl enable fail2ban.service
$ sudo fail2ban-client status
"Status
|- Number of jail:      1
`- Jail list:   sshd"

#Commande pour débloquer une ip
$ fail2ban-client set sshd unbanip 192.168.1.12 

```
- HTTPS
    - Génération du certificat et de la clé publique

```bash 
$ sudo openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt
[sudo] password for theo:
Generating a 2048 bit RSA private key
..+++
.....+++
writing new private key to 'web.cesi.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Gironde
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:cesi
Organizational Unit Name (eg, section) []:cesi
Common Name (eg, your name or your server's hostname) []:web.cesi
Email Address []:'

$ sudo mv /home/theo/web.cesi.crt /etc/pki/tls/certs/
$ ls /etc/pki/tls/certs/
"ca-bundle.crt  ca-bundle.trust.crt  make-dummy-cert  Makefile  renew-dummy-cert  web.cesi.crt"
$ sudo mv /home/theo/web.cesi.key /etc/pki/tls/private/
$ ls /etc/pki/tls/private/
"web.cesi.key"
```

```bash=
$ sudo vi /etc/nginx/nginx.conf
listen    443
$ sudo nginx -t
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful

$ sudo systemctl restart nginx
$ ss -alnpt
LISTEN     0      128                                                                          *:80                                                                                                                             *:*
LISTEN     0      128                                                                          *:443 

# Certificat + redirection http->https
$ sudo vim /etc/nginx/nginx.conf
events {}

http {
   server {

        listen      443;
        server_name web.cesi;
        ssl on;
        ssl_certificate "/etc/pki/tls/certs/web.cesi.crt";
        ssl_certificate_key     "/etc/pki/tls/private/web.cesi.key";
        location / {
            proxy_pass   http://10.55.55.11;
        }
    }
   server{

        listen 80 default_server;
        server_name web.cesi;
        return 301 https://$host$request_uri;
         }
}

$ sudo systemctl restart nginx


```

- Monitoring

```bash    

$ bash <(curl -Ss https://my-netdata.io/kickstart.sh)
$ sudo firewall-cmd --add-port=19999/tcp --permanent
"success"
$ sudo systemctl restart firewalld
```
* Une fois le webhook crée sur discord, nous pouvons créer notre fichier health_alarm_notify.conf avec la configuration ci-dessous :
```bash    
$ sudo vi /etc/netdata/health_alarm_notify.conf
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/789110476306251846/rspW8iJbVv6_xzr0sdR1Bdl7qHvzROAaG_Hs4WmLE_bABOSQR_x9PxwqV4wof3PFe1QO"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"

```
* Nous recevons bien par la suite une notification sur le serveur discord quand la ram est à un niveau critique : 

***rp.tp2.cesi is critical, system.swap (swap), ram in swap = 57.8% of RAM***
