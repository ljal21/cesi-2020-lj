#      TP Linux 3 (Avtan, Alexandre, Théo, Léo)

##    Mise en place DNS

```bash=

$ sudo firewall-cmd --permanent --add-service=dns
$ sudo firewall-cmd --reload
$ sudo firewall-cmd --list-all


$ sudo yum -y install epel-release
$ sudo yum install bind bind-utils
$ sudo mv /etc/named.conf /etc/named.conf.orig
```
- Fichier de Configuration 

```bash
$ sudo vim /etc/named.conf
"//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//
// See the BIND Administrator's Reference Manual (ARM) for details about the
// configuration located in /usr/share/doc/bind-{version}/Bv9ARM.html

options {
        listen-on port 53 { 127.0.0.1; 10.55.55.13; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        recursing-file  "/var/named/data/named.recursing";
        secroots-file   "/var/named/data/named.secroots";
        allow-query     { 10.55.55.0/24; };

         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-enable yes;
        dnssec-validation yes;

        /* Path to ISC DLV key */
        bindkeys-file "/etc/named.root.key";

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
};

zone "tp2.cesi" IN {

         type master;

         file "/var/named/tp2.cesi.db";

         allow-update { none; };
};

zone "55.55.10.in-addr.arpa" IN {

          type master;

          file "/var/named/55.55.10.db";

          allow-update { none; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";"

```

- Fichier de Configuration tp2.cesi.db

```bash
$ sudo vim /var/named/tp2.cesi.db
$TTL    604800
@   IN  SOA     ns1.tp2.cesi. root.tp2.cesi. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@      IN  NS      ns1.tp2.cesi.

ns1 IN  A       10.55.55.13
web     IN  A       10.55.55.11
db     IN  A       10.55.55.12
rp     IN  A       10.55.55.10

```
- Fichier de Configuration 55.55.10.db
```bash
$ sudo vim /var/named/55.55.10.db
$TTL    604800
@   IN  SOA     ns1.tp2.cesi. root.tp2.cesi. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@ IN  NS      ns1.tp2.cesi.

13.55.55.10 IN PTR ns1.tp2.cesi.

;PTR Record IP address to HostName
11      IN  PTR     web.tp2.cesi.
12      IN  PTR     db.tp2.cesi.
10      IN  PTR     rp.tp2.cesi.
```

```bash=
$ sudo systemctl start named
$ sudo systemctl enable named
```
- Test sur machine distante
```bash
$ dig db.tp2.cesi @10.55.55.13

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> db.tp2.cesi @10.55.55.13
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62764
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;db.tp2.cesi.                   IN      A

;; ANSWER SECTION:
db.tp2.cesi.            604800  IN      A       10.55.55.12

;; AUTHORITY SECTION:
tp2.cesi.               604800  IN      NS      ns1.tp2.cesi.

;; ADDITIONAL SECTION:
ns1.tp2.cesi.           604800  IN      A       10.55.55.13

;; Query time: 0 msec
;; SERVER: 10.55.55.13#53(10.55.55.13)
;; WHEN: Fri Dec 18 10:18:20 CET 2020
;; MSG SIZE  rcvd: 90

```
- Rajout permanent du DNS
```bash=
$ sudo vim /etc/resolv.conf
"# Generated by NetworkManager
search localdomain tp2.cesi
#nameserver 192.168.230.2
nameserver 10.55.55.13"
#Test de ping depuis machine distante
$ ping ns1
PING ns1.tp2.cesi (10.55.55.13) 56(84) bytes of data.
64 bytes from rp.tp2.cesi (10.55.55.13): icmp_seq=1 ttl=64 time=0.322 ms
64 bytes from rp.tp2.cesi (10.55.55.13): icmp_seq=2 ttl=64 time=0.413 ms

```



